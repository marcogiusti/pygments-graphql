import unittest
import textwrap
from pygments.token import (
    Comment, Error, Keyword, Name, Number, Punctuation, String, Text
)
from pygments_graphql import GraphqlLexer


def assert_tokens_equal(self, text, expected, stack=None):
    text = textwrap.dedent(text)
    lexer = GraphqlLexer()
    if stack:
        itokens = lexer.get_tokens_unprocessed(text, stack=stack)
    else:
        itokens = lexer.get_tokens_unprocessed(text)
    self.assertEqual(list(itokens), expected)


def assert_value_equal(self, text, expected):
    assert_tokens_equal(self, text, [expected], stack=('value', ))


def assert_string_equal(self, text, tokens):
    start = (0, String, '"')
    end = (len(text) - 1, String, '"')
    expected = [start, *tokens, end]
    assert_tokens_equal(self, text, expected, stack=('value', ))


class TestValue(unittest.TestCase):

    maxDiff = None

    def test_float(self):
        assert_value_equal(self, '5.5', (0, Number.Float, '5.5'))
        assert_value_equal(self, '5e3', (0, Number.Float, '5e3'))
        assert_value_equal(self, '5E3', (0, Number.Float, '5E3'))
        assert_value_equal(self, '5e-3', (0, Number.Float, '5e-3'))
        assert_value_equal(self, '5E-3', (0, Number.Float, '5E-3'))
        assert_value_equal(self, '5e+3', (0, Number.Float, '5e+3'))
        assert_value_equal(self, '5E+3', (0, Number.Float, '5E+3'))
        assert_value_equal(self, '5.1e3', (0, Number.Float, '5.1e3'))
        assert_value_equal(self, '5.1E3', (0, Number.Float, '5.1E3'))
        assert_value_equal(self, '5.1e-3', (0, Number.Float, '5.1e-3'))
        assert_value_equal(self, '5.1E-3', (0, Number.Float, '5.1E-3'))
        assert_value_equal(self, '5.1e+3', (0, Number.Float, '5.1e+3'))
        assert_value_equal(self, '5.1E+3', (0, Number.Float, '5.1E+3'))

    def test_integer(self):
        assert_value_equal(self, '5', (0, Number.Integer, '5'))
        assert_value_equal(self, '-0', (0, Number.Integer, '-0'))
        # Not really following the specifications
        assert_value_equal(self, '-02', (0, Number.Integer, '-02'))

    def test_string(self):
        assert_string_equal(self, '"asdf"', [(1, String, 'asdf')])
        assert_string_equal(self, r'"asdf\""', [
            (1, String, 'asdf'), (5, String.Escape, r'\"'),
        ])
        assert_string_equal(self, r'"asdf\m"', [
            (1, String, 'asdf'), (5, Error, '\\'), (6, String, 'm')
        ])
        assert_string_equal(self, r'"asdf\u00E8"', [
            (1, String, 'asdf'), (5, String.Escape, r'\u00E8'),
        ])

    def test_boolean(self):
        assert_value_equal(self, 'true', (0, Name.Builtin, 'true'))
        assert_value_equal(self, 'false', (0, Name.Builtin, 'false'))

    def test_variable(self):
        assert_value_equal(self, '$hello', (0, Name.Variable, '$hello'))
        assert_tokens_equal(self, '$0hello', [
            (0, Error, '$'),
            (1, Number.Integer, '0'),
            (2, Name.Constant, 'hello'),
        ], stack=('value', ))

    def test_list_value(self):
        assert_tokens_equal(self, '[1, 1.1, "abc"]', [
            (0, Punctuation, '['),
            (1, Number.Integer, '1'),
            (2, Punctuation, ','),
            (3, Text, ' '),
            (4, Number.Float, '1.1'),
            (7, Punctuation, ','),
            (8, Text, ' '),
            (9, String, '"'),
            (10, String, 'abc'),
            (13, String, '"'),
            (14, Punctuation, ']'),
        ], stack=('value', ))
        assert_tokens_equal(self, '[1, 1.1, ["abc"]]', [
            (0, Punctuation, '['),
            (1, Number.Integer, '1'),
            (2, Punctuation, ','),
            (3, Text, ' '),
            (4, Number.Float, '1.1'),
            (7, Punctuation, ','),
            (8, Text, ' '),
            (9, Punctuation, '['),
            (10, String, '"'),
            (11, String, 'abc'),
            (14, String, '"'),
            (15, Punctuation, ']'),
            (16, Punctuation, ']'),
        ], stack=('value', ))


class TestGraphqlLexer(unittest.TestCase):

    maxDiff = None

    def assert_builtin(self, text):
        assert_tokens_equal(self, text, [(0, Name.Builtin, text)],
                            stack=('builtins', ))

    def test_keyworkd(self):
        assert_tokens_equal(self, 'type', [(0, Keyword, 'type')])
        assert_tokens_equal(self, 'schema', [(0, Keyword, 'schema')])
        assert_tokens_equal(self, 'extend', [(0, Keyword, 'extend')])
        assert_tokens_equal(self, 'enum', [(0, Keyword, 'enum')])
        assert_tokens_equal(self, 'scalar', [(0, Keyword, 'scalar')])
        assert_tokens_equal(self, 'implements', [(0, Keyword, 'implements')])
        assert_tokens_equal(self, 'interface', [(0, Keyword, 'interface')])
        assert_tokens_equal(self, 'union', [(0, Keyword, 'union')])
        assert_tokens_equal(self, 'input', [(0, Keyword, 'input')])
        assert_tokens_equal(self, 'directive', [(0, Keyword, 'directive')])

    def test_arguments_integer(self):
        text = '''\
        {
          field(arg: 24)
          field
        }
        '''

        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'field'),
            (9, Punctuation, '('),
            (10, Name, 'arg'),
            # (10, Name.Attribute, 'arg'),
            (13, Punctuation, ':'),
            (14, Text, ' '),
            (15, Number.Integer, '24'),
            (17, Punctuation, ')'),
            (18, Text, '\n  '),
            (21, Name, 'field'),
            (26, Text, '\n'),
            (27, Punctuation, '}'),
            (28, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_simple(self):
        text = '''\
        {
          me {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'me'),
            (6, Text, ' '),
            (7, Punctuation, '{'),
            (8, Text, '\n    '),
            (13, Name, 'name'),
            (17, Text, '\n  '),
            (20, Punctuation, '}'),
            (21, Text, '\n'),
            (22, Punctuation, '}'),
            (23, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_comment(self):
        text = '''\
        {
          hero {
            name
            # Queries can have comments!
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'hero'),
            (8, Text, ' '),
            (9, Punctuation, '{'),
            (10, Text, '\n    '),
            (15, Name, 'name'),
            (19, Text, '\n    '),
            (24, Comment, '# Queries can have comments!'),
            (52, Text, '\n    '),
            (57, Name, 'friends'),
            (64, Text, ' '),
            (65, Punctuation, '{'),
            (66, Text, '\n      '),
            (73, Name, 'name'),
            (77, Text, '\n    '),
            (82, Punctuation, '}'),
            (83, Text, '\n  '),
            (86, Punctuation, '}'),
            (87, Text, '\n'),
            (88, Punctuation, '}'),
            (89, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_arguments(self):
        text = '''\
        {
          human(id: "1000") {
            name
            height
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'human'),
            (9, Punctuation, '('),
            # (10, Name.Attribute, 'id'),
            (10, Name, 'id'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, String, '"'),
            (15, String, '1000'),
            (19, String, '"'),
            (20, Punctuation, ')'),
            (21, Text, ' '),
            (22, Punctuation, '{'),
            (23, Text, '\n    '),
            (28, Name, 'name'),
            (32, Text, '\n    '),
            (37, Name, 'height'),
            (43, Text, '\n  '),
            (46, Punctuation, '}'),
            (47, Text, '\n'),
            (48, Punctuation, '}'),
            (49, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_argument_scalar(self):
        text = '''\
        {
          human(id: "1000") {
            name
            height(unit: FOOT)
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'human'),
            (9, Punctuation, '('),
            # (10, Name.Attribute, 'id'),
            (10, Name, 'id'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, String, '"'),
            (15, String, '1000'),
            (19, String, '"'),
            (20, Punctuation, ')'),
            (21, Text, ' '),
            (22, Punctuation, '{'),
            (23, Text, '\n    '),
            (28, Name, 'name'),
            (32, Text, '\n    '),
            (37, Name, 'height'),
            (43, Punctuation, '('),
            # (44, Name.Attribute, 'unit'),
            (44, Name, 'unit'),
            (48, Punctuation, ':'),
            (49, Text, ' '),
            (50, Name.Constant, 'FOOT'),
            (54, Punctuation, ')'),
            (55, Text, '\n  '),
            (58, Punctuation, '}'),
            (59, Text, '\n'),
            (60, Punctuation, '}'),
            (61, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_alias(self):
        text = '''\
        {
          empireHero: hero(episode: EMPIRE) {
            name
          }
          jediHero: hero(episode: JEDI) {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name.Label, 'empireHero'),
            (14, Punctuation, ':'),
            (15, Text, ' '),
            (16, Name, 'hero'),
            (20, Punctuation, '('),
            # (21, Name.Attribute, 'episode'),
            (21, Name, 'episode'),
            (28, Punctuation, ':'),
            (29, Text, ' '),
            (30, Name.Constant, 'EMPIRE'),
            (36, Punctuation, ')'),
            (37, Text, ' '),
            (38, Punctuation, '{'),
            (39, Text, '\n    '),
            (44, Name, 'name'),
            (48, Text, '\n  '),
            (51, Punctuation, '}'),
            (52, Text, '\n  '),
            (55, Name.Label, 'jediHero'),
            (63, Punctuation, ':'),
            (64, Text, ' '),
            (65, Name, 'hero'),
            (69, Punctuation, '('),
            # (70, Name.Attribute, 'episode'),
            (70, Name, 'episode'),
            (77, Punctuation, ':'),
            (78, Text, ' '),
            (79, Name.Constant, 'JEDI'),
            (83, Punctuation, ')'),
            (84, Text, ' '),
            (85, Punctuation, '{'),
            (86, Text, '\n    '),
            (91, Name, 'name'),
            (95, Text, '\n  '),
            (98, Punctuation, '}'),
            (99, Text, '\n'),
            (100, Punctuation, '}'),
            (101, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_variables(self):
        text = '''\
        mutation testVar($int: Int, $myType: MyType,
            $required: String!, $list: [Int!]!)
        {
          doSomenthing(int: $int, myType: $myType,
              required: $required, list: $list)
          {
            response
          }
        }
        '''
        expected = [
            (0, Keyword, 'mutation'),
            (8, Text, ' '),
            (9, Name.Function, 'testVar'),
            (16, Punctuation, '('),
            (17, Name.Variable, '$int'),
            (21, Punctuation, ':'),
            (22, Text, ' '),
            (23, Name.Builtin, 'Int'),
            (26, Punctuation, ','),
            (27, Text, ' '),
            (28, Name.Variable, '$myType'),
            (35, Punctuation, ':'),
            (36, Text, ' '),
            (37, Name.Class, 'MyType'),
            (43, Punctuation, ','),
            (44, Text, '\n    '),
            (49, Name.Variable, '$required'),
            (58, Punctuation, ':'),
            (59, Text, ' '),
            (60, Name.Builtin, 'String'),
            (66, Punctuation, '!'),
            (67, Punctuation, ','),
            (68, Text, ' '),
            (69, Name.Variable, '$list'),
            (74, Punctuation, ':'),
            (75, Text, ' '),
            (76, Punctuation, '['),
            (77, Name.Builtin, 'Int'),
            (80, Punctuation, '!'),
            (81, Punctuation, ']'),
            (82, Punctuation, '!'),
            (83, Punctuation, ')'),
            (84, Text, '\n'),
            (85, Punctuation, '{'),
            (86, Text, '\n  '),
            (89, Name, 'doSomenthing'),
            (101, Punctuation, '('),
            (102, Name, 'int'),
            (105, Punctuation, ':'),
            (106, Text, ' '),
            (107, Name.Variable, '$int'),
            (111, Punctuation, ','),
            (112, Text, ' '),
            (113, Name, 'myType'),
            (119, Punctuation, ':'),
            (120, Text, ' '),
            (121, Name.Variable, '$myType'),
            (128, Punctuation, ','),
            (129, Text, '\n      '),
            (136, Name, 'required'),
            (144, Punctuation, ':'),
            (145, Text, ' '),
            (146, Name.Variable, '$required'),
            (155, Punctuation, ','),
            (156, Text, ' '),
            (157, Name, 'list'),
            (161, Punctuation, ':'),
            (162, Text, ' '),
            (163, Name.Variable, '$list'),
            (168, Punctuation, ')'),
            (169, Text, '\n  '),
            (172, Punctuation, '{'),
            (173, Text, '\n    '),
            (178, Name, 'response'),
            (186, Text, '\n  '),
            (189, Punctuation, '}'),
            (190, Text, '\n'),
            (191, Punctuation, '}'),
            (192, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_operation_definition_directives(self):
        text = '''\
        query ($foo: Boolean = true, $bar: Boolean = false) {
          field @skip(if: $foo) {
            subfieldA
          }
          field @skip(if: $bar) {
            subfieldB
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Punctuation, '('),
            (7, Name.Variable, '$foo'),
            (11, Punctuation, ':'),
            (12, Text, ' '),
            (13, Name.Builtin, 'Boolean'),
            (20, Text, ' '),
            (21, Punctuation, '='),
            (22, Text, ' '),
            (23, Name.Builtin, 'true'),
            (27, Punctuation, ','),
            (28, Text, ' '),
            (29, Name.Variable, '$bar'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Builtin, 'Boolean'),
            (42, Text, ' '),
            (43, Punctuation, '='),
            (44, Text, ' '),
            (45, Name.Builtin, 'false'),
            (50, Punctuation, ')'),
            (51, Text, ' '),
            (52, Punctuation, '{'),
            (53, Text, '\n  '),
            (56, Name, 'field'),
            (61, Text, ' '),
            (62, Name.Decorator, '@skip'),
            (67, Punctuation, '('),
            (68, Name, 'if'),
            (70, Punctuation, ':'),
            (71, Text, ' '),
            (72, Name.Variable, '$foo'),
            (76, Punctuation, ')'),
            (77, Text, ' '),
            (78, Punctuation, '{'),
            (79, Text, '\n    '),
            (84, Name, 'subfieldA'),
            (93, Text, '\n  '),
            (96, Punctuation, '}'),
            (97, Text, '\n  '),
            (100, Name, 'field'),
            (105, Text, ' '),
            (106, Name.Decorator, '@skip'),
            (111, Punctuation, '('),
            (112, Name, 'if'),
            (114, Punctuation, ':'),
            (115, Text, ' '),
            (116, Name.Variable, '$bar'),
            (120, Punctuation, ')'),
            (121, Text, ' '),
            (122, Punctuation, '{'),
            (123, Text, '\n    '),
            (128, Name, 'subfieldB'),
            (137, Text, '\n  '),
            (140, Punctuation, '}'),
            (141, Text, '\n'),
            (142, Punctuation, '}'),
            (143, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_operation_definition_multi_directives(self):
        text = '''\
        query ($foo: Boolean = true, $bar: Boolean = false) {
          field @skip(if: $foo) @skip(if: $bar)
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Punctuation, '('),
            (7, Name.Variable, '$foo'),
            (11, Punctuation, ':'),
            (12, Text, ' '),
            (13, Name.Builtin, 'Boolean'),
            (20, Text, ' '),
            (21, Punctuation, '='),
            (22, Text, ' '),
            (23, Name.Builtin, 'true'),
            (27, Punctuation, ','),
            (28, Text, ' '),
            (29, Name.Variable, '$bar'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Builtin, 'Boolean'),
            (42, Text, ' '),
            (43, Punctuation, '='),
            (44, Text, ' '),
            (45, Name.Builtin, 'false'),
            (50, Punctuation, ')'),
            (51, Text, ' '),
            (52, Punctuation, '{'),
            (53, Text, '\n  '),
            (56, Name, 'field'),
            (61, Text, ' '),
            (62, Name.Decorator, '@skip'),
            (67, Punctuation, '('),
            (68, Name, 'if'),
            (70, Punctuation, ':'),
            (71, Text, ' '),
            (72, Name.Variable, '$foo'),
            (76, Punctuation, ')'),
            (77, Text, ' '),
            (78, Name.Decorator, '@skip'),
            (83, Punctuation, '('),
            (84, Name, 'if'),
            (86, Punctuation, ':'),
            (87, Text, ' '),
            (88, Name.Variable, '$bar'),
            (92, Punctuation, ')'),
            (93, Text, '\n'),
            (94, Punctuation, '}'),
            (95, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_fragment_spread(self):
        text = '''\
        query withNestedFragments {
          user(id: 4) {
            friends(first: 10) {
              ...friendFields
            }
            mutualFriends(first: 10) {
              ...friendFields @skip(if: true)
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'withNestedFragments'),
            (25, Text, ' '),
            (26, Punctuation, '{'),
            (27, Text, '\n  '),
            (30, Name, 'user'),
            (34, Punctuation, '('),
            (35, Name, 'id'),
            (37, Punctuation, ':'),
            (38, Text, ' '),
            (39, Number.Integer, '4'),
            (40, Punctuation, ')'),
            (41, Text, ' '),
            (42, Punctuation, '{'),
            (43, Text, '\n    '),
            (48, Name, 'friends'),
            (55, Punctuation, '('),
            (56, Name, 'first'),
            (61, Punctuation, ':'),
            (62, Text, ' '),
            (63, Number.Integer, '10'),
            (65, Punctuation, ')'),
            (66, Text, ' '),
            (67, Punctuation, '{'),
            (68, Text, '\n      '),
            (75, Punctuation, '...'),
            (78, Name, 'friendFields'),
            (90, Text, '\n    '),
            (95, Punctuation, '}'),
            (96, Text, '\n    '),
            (101, Name, 'mutualFriends'),
            (114, Punctuation, '('),
            (115, Name, 'first'),
            (120, Punctuation, ':'),
            (121, Text, ' '),
            (122, Number.Integer, '10'),
            (124, Punctuation, ')'),
            (125, Text, ' '),
            (126, Punctuation, '{'),
            (127, Text, '\n      '),
            (134, Punctuation, '...'),
            (137, Name, 'friendFields'),
            (149, Text, ' '),
            (150, Name.Decorator, '@skip'),
            (155, Punctuation, '('),
            (156, Name, 'if'),
            (158, Punctuation, ':'),
            (159, Text, ' '),
            (160, Name.Builtin, 'true'),
            (164, Punctuation, ')'),
            (165, Text, '\n    '),
            (170, Punctuation, '}'),
            (171, Text, '\n  '),
            (174, Punctuation, '}'),
            (175, Text, '\n'),
            (176, Punctuation, '}'),
            (177, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_inline_fragment(self):
        text = '''\
        query inlineFragmentTyping {
          profiles(handles: ["zuck", "cocacola"]) {
            handle
            ... on User {
              friends {
                count
              }
            }
            ... on Page {
              likers {
                count
              }
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'inlineFragmentTyping'),
            (26, Text, ' '),
            (27, Punctuation, '{'),
            (28, Text, '\n  '),
            (31, Name, 'profiles'),
            (39, Punctuation, '('),
            (40, Name, 'handles'),
            (47, Punctuation, ':'),
            (48, Text, ' '),
            (49, Punctuation, '['),
            (50, String, '"'),
            (51, String, 'zuck'),
            (55, String, '"'),
            (56, Punctuation, ','),
            (57, Text, ' '),
            (58, String, '"'),
            (59, String, 'cocacola'),
            (67, String, '"'),
            (68, Punctuation, ']'),
            (69, Punctuation, ')'),
            (70, Text, ' '),
            (71, Punctuation, '{'),
            (72, Text, '\n    '),
            (77, Name, 'handle'),
            (83, Text, '\n    '),
            (88, Punctuation, '...'),
            (91, Text, ' '),
            (92, Keyword, 'on'),
            (94, Text, ' '),
            (95, Name.Class, 'User'),
            (99, Text, ' '),
            (100, Punctuation, '{'),
            (101, Text, '\n      '),
            (108, Name, 'friends'),
            (115, Text, ' '),
            (116, Punctuation, '{'),
            (117, Text, '\n        '),
            (126, Name, 'count'),
            (131, Text, '\n      '),
            (138, Punctuation, '}'),
            (139, Text, '\n    '),
            (144, Punctuation, '}'),
            (145, Text, '\n    '),
            (150, Punctuation, '...'),
            (153, Text, ' '),
            (154, Keyword, 'on'),
            (156, Text, ' '),
            (157, Name.Class, 'Page'),
            (161, Text, ' '),
            (162, Punctuation, '{'),
            (163, Text, '\n      '),
            (170, Name, 'likers'),
            (176, Text, ' '),
            (177, Punctuation, '{'),
            (178, Text, '\n        '),
            (187, Name, 'count'),
            (192, Text, '\n      '),
            (199, Punctuation, '}'),
            (200, Text, '\n    '),
            (205, Punctuation, '}'),
            (206, Text, '\n  '),
            (209, Punctuation, '}'),
            (210, Text, '\n'),
            (211, Punctuation, '}'),
            (212, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_fragment_definition(self):
        text = '''\
        {
          leftComparison: hero(episode: EMPIRE) {
            ...comparisonFields
          }
          rightComparison: hero(episode: JEDI) {
            ...comparisonFields
          }
        }

        fragment comparisonFields on Character {
          name
          appearsIn
          friends {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name.Label, 'leftComparison'),
            (18, Punctuation, ':'),
            (19, Text, ' '),
            (20, Name, 'hero'),
            (24, Punctuation, '('),
            # (25, Name.Attribute, 'episode'),
            (25, Name, 'episode'),
            (32, Punctuation, ':'),
            (33, Text, ' '),
            (34, Name.Constant, 'EMPIRE'),
            (40, Punctuation, ')'),
            (41, Text, ' '),
            (42, Punctuation, '{'),
            (43, Text, '\n    '),
            (48, Punctuation, '...'),
            # (51, Name.Function, 'comparisonFields'),
            (51, Name, 'comparisonFields'),
            (67, Text, '\n  '),
            (70, Punctuation, '}'),
            (71, Text, '\n  '),
            (74, Name.Label, 'rightComparison'),
            (89, Punctuation, ':'),
            (90, Text, ' '),
            (91, Name, 'hero'),
            (95, Punctuation, '('),
            # (96, Name.Attribute, 'episode'),
            (96, Name, 'episode'),
            (103, Punctuation, ':'),
            (104, Text, ' '),
            (105, Name.Constant, 'JEDI'),
            (109, Punctuation, ')'),
            (110, Text, ' '),
            (111, Punctuation, '{'),
            (112, Text, '\n    '),
            (117, Punctuation, '...'),
            # (120, Name.Function, 'comparisonFields'),
            (120, Name, 'comparisonFields'),
            (136, Text, '\n  '),
            (139, Punctuation, '}'),
            (140, Text, '\n'),
            (141, Punctuation, '}'),
            (142, Text, '\n\n'),
            (144, Keyword, 'fragment'),
            (152, Text, ' '),
            (153, Name.Function, 'comparisonFields'),
            (169, Text, ' '),
            (170, Keyword, 'on'),
            (172, Text, ' '),
            (173, Name.Class, 'Character'),
            (182, Text, ' '),
            (183, Punctuation, '{'),
            (184, Text, '\n  '),
            (187, Name, 'name'),
            (191, Text, '\n  '),
            (194, Name, 'appearsIn'),
            (203, Text, '\n  '),
            (206, Name, 'friends'),
            (213, Text, ' '),
            (214, Punctuation, '{'),
            (215, Text, '\n    '),
            (220, Name, 'name'),
            (224, Text, '\n  '),
            (227, Punctuation, '}'),
            (228, Text, '\n'),
            (229, Punctuation, '}'),
            (230, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_fragment_variables(self):
        text = '''\
        query HeroComparison($first: Int = 3) {
          leftComparison: hero(episode: EMPIRE) {
            ...comparisonFields
          }
          rightComparison: hero(episode: JEDI) {
            ...comparisonFields
          }
        }

        fragment comparisonFields on Character {
          name
          friendsConnection(first: $first) {
            totalCount
            edges {
              node {
                name
              }
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroComparison'),
            (20, Punctuation, '('),
            (21, Name.Variable, '$first'),
            (27, Punctuation, ':'),
            (28, Text, ' '),
            (29, Name.Builtin, 'Int'),
            (32, Text, ' '),
            (33, Punctuation, '='),
            (34, Text, ' '),
            (35, Number.Integer, '3'),
            (36, Punctuation, ')'),
            (37, Text, ' '),
            (38, Punctuation, '{'),
            (39, Text, '\n  '),
            (42, Name.Label, 'leftComparison'),
            (56, Punctuation, ':'),
            (57, Text, ' '),
            (58, Name, 'hero'),
            (62, Punctuation, '('),
            # (63, Name.Attribute, 'episode'),
            (63, Name, 'episode'),
            (70, Punctuation, ':'),
            (71, Text, ' '),
            (72, Name.Constant, 'EMPIRE'),
            (78, Punctuation, ')'),
            (79, Text, ' '),
            (80, Punctuation, '{'),
            (81, Text, '\n    '),
            (86, Punctuation, '...'),
            # (89, Name.Function, 'comparisonFields'),
            (89, Name, 'comparisonFields'),
            (105, Text, '\n  '),
            (108, Punctuation, '}'),
            (109, Text, '\n  '),
            (112, Name.Label, 'rightComparison'),
            (127, Punctuation, ':'),
            (128, Text, ' '),
            (129, Name, 'hero'),
            (133, Punctuation, '('),
            # (134, Name.Attribute, 'episode'),
            (134, Name, 'episode'),
            (141, Punctuation, ':'),
            (142, Text, ' '),
            (143, Name.Constant, 'JEDI'),
            (147, Punctuation, ')'),
            (148, Text, ' '),
            (149, Punctuation, '{'),
            (150, Text, '\n    '),
            (155, Punctuation, '...'),
            # (158, Name.Function, 'comparisonFields'),
            (158, Name, 'comparisonFields'),
            (174, Text, '\n  '),
            (177, Punctuation, '}'),
            (178, Text, '\n'),
            (179, Punctuation, '}'),
            (180, Text, '\n\n'),
            (182, Keyword, 'fragment'),
            (190, Text, ' '),
            (191, Name.Function, 'comparisonFields'),
            (207, Text, ' '),
            (208, Keyword, 'on'),
            (210, Text, ' '),
            (211, Name.Class, 'Character'),
            (220, Text, ' '),
            (221, Punctuation, '{'),
            (222, Text, '\n  '),
            (225, Name, 'name'),
            (229, Text, '\n  '),
            (232, Name, 'friendsConnection'),
            (249, Punctuation, '('),
            # (250, Name.Attribute, 'first'),
            (250, Name, 'first'),
            (255, Punctuation, ':'),
            (256, Text, ' '),
            (257, Name.Variable, '$first'),
            (263, Punctuation, ')'),
            (264, Text, ' '),
            (265, Punctuation, '{'),
            (266, Text, '\n    '),
            (271, Name, 'totalCount'),
            (281, Text, '\n    '),
            (286, Name, 'edges'),
            (291, Text, ' '),
            (292, Punctuation, '{'),
            (293, Text, '\n      '),
            (300, Name, 'node'),
            (304, Text, ' '),
            (305, Punctuation, '{'),
            (306, Text, '\n        '),
            (315, Name, 'name'),
            (319, Text, '\n      '),
            (326, Punctuation, '}'),
            (327, Text, '\n    '),
            (332, Punctuation, '}'),
            (333, Text, '\n  '),
            (336, Punctuation, '}'),
            (337, Text, '\n'),
            (338, Punctuation, '}'),
            (339, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_operation_name(self):
        text = '''\
        query HeroNameAndFriends {
          hero {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Text, ' '),
            (25, Punctuation, '{'),
            (26, Text, '\n  '),
            (29, Name, 'hero'),
            (33, Text, ' '),
            (34, Punctuation, '{'),
            (35, Text, '\n    '),
            (40, Name, 'name'),
            (44, Text, '\n    '),
            (49, Name, 'friends'),
            (56, Text, ' '),
            (57, Punctuation, '{'),
            (58, Text, '\n      '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '}'),
            (75, Text, '\n  '),
            (78, Punctuation, '}'),
            (79, Text, '\n'),
            (80, Punctuation, '}'),
            (81, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_query_variables(self):
        text = '''\
        query HeroNameAndFriends($episode: Episode) {
          hero(episode: $episode) {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Punctuation, '('),
            (25, Name.Variable, '$episode'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Class, 'Episode'),
            (42, Punctuation, ')'),
            (43, Text, ' '),
            (44, Punctuation, '{'),
            (45, Text, '\n  '),
            (48, Name, 'hero'),
            (52, Punctuation, '('),
            # (53, Name.Attribute, 'episode'),
            (53, Name, 'episode'),
            (60, Punctuation, ':'),
            (61, Text, ' '),
            (62, Name.Variable, '$episode'),
            (70, Punctuation, ')'),
            (71, Text, ' '),
            (72, Punctuation, '{'),
            (73, Text, '\n    '),
            (78, Name, 'name'),
            (82, Text, '\n    '),
            (87, Name, 'friends'),
            (94, Text, ' '),
            (95, Punctuation, '{'),
            (96, Text, '\n      '),
            (103, Name, 'name'),
            (107, Text, '\n    '),
            (112, Punctuation, '}'),
            (113, Text, '\n  '),
            (116, Punctuation, '}'),
            (117, Text, '\n'),
            (118, Punctuation, '}'),
            (119, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_query_variables_default_values(self):
        text = '''\
        query HeroNameAndFriends($episode: Episode = JEDI) {
          hero(episode: $episode) {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Punctuation, '('),
            (25, Name.Variable, '$episode'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Class, 'Episode'),
            (42, Text, ' '),
            (43, Punctuation, '='),
            (44, Text, ' '),
            (45, Name.Constant, 'JEDI'),
            (49, Punctuation, ')'),
            (50, Text, ' '),
            (51, Punctuation, '{'),
            (52, Text, '\n  '),
            (55, Name, 'hero'),
            (59, Punctuation, '('),
            # (60, Name.Attribute, 'episode'),
            (60, Name, 'episode'),
            (67, Punctuation, ':'),
            (68, Text, ' '),
            (69, Name.Variable, '$episode'),
            (77, Punctuation, ')'),
            (78, Text, ' '),
            (79, Punctuation, '{'),
            (80, Text, '\n    '),
            (85, Name, 'name'),
            (89, Text, '\n    '),
            (94, Name, 'friends'),
            (101, Text, ' '),
            (102, Punctuation, '{'),
            (103, Text, '\n      '),
            (110, Name, 'name'),
            (114, Text, '\n    '),
            (119, Punctuation, '}'),
            (120, Text, '\n  '),
            (123, Punctuation, '}'),
            (124, Text, '\n'),
            (125, Punctuation, '}'),
            (126, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_inline_fragments(self):
        text = '''\
        query HeroForEpisode($ep: Episode!) {
          hero(episode: $ep) {
            name
            ... on Droid {
              primaryFunction
            }
            ... on Human {
              height
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroForEpisode'),
            (20, Punctuation, '('),
            (21, Name.Variable, '$ep'),
            (24, Punctuation, ':'),
            (25, Text, ' '),
            (26, Name.Class, 'Episode'),
            (33, Punctuation, '!'),
            (34, Punctuation, ')'),
            (35, Text, ' '),
            (36, Punctuation, '{'),
            (37, Text, '\n  '),
            (40, Name, 'hero'),
            (44, Punctuation, '('),
            # (45, Name.Attribute, 'episode'),
            (45, Name, 'episode'),
            (52, Punctuation, ':'),
            (53, Text, ' '),
            (54, Name.Variable, '$ep'),
            (57, Punctuation, ')'),
            (58, Text, ' '),
            (59, Punctuation, '{'),
            (60, Text, '\n    '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '...'),
            (77, Text, ' '),
            (78, Keyword, 'on'),
            (80, Text, ' '),
            (81, Name.Class, 'Droid'),
            (86, Text, ' '),
            (87, Punctuation, '{'),
            (88, Text, '\n      '),
            (95, Name, 'primaryFunction'),
            (110, Text, '\n    '),
            (115, Punctuation, '}'),
            (116, Text, '\n    '),
            (121, Punctuation, '...'),
            (124, Text, ' '),
            (125, Keyword, 'on'),
            (127, Text, ' '),
            (128, Name.Class, 'Human'),
            (133, Text, ' '),
            (134, Punctuation, '{'),
            (135, Text, '\n      '),
            (142, Name, 'height'),
            (148, Text, '\n    '),
            (153, Punctuation, '}'),
            (154, Text, '\n  '),
            (157, Punctuation, '}'),
            (158, Text, '\n'),
            (159, Punctuation, '}'),
            (160, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_meta_fields(self):
        text = '''\
        {
          search(text: "an") {
            __typename
            ... on Human {
              name
            }
            ... on Droid {
              name
            }
            ... on Starship {
              name
            }
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'search'),
            (10, Punctuation, '('),
            # (11, Name.Attribute, 'text'),
            (11, Name, 'text'),
            (15, Punctuation, ':'),
            (16, Text, ' '),
            (17, String, '"'),
            (18, String, 'an'),
            (20, String, '"'),
            (21, Punctuation, ')'),
            (22, Text, ' '),
            (23, Punctuation, '{'),
            (24, Text, '\n    '),
            (29, Name, '__typename'),
            (39, Text, '\n    '),
            (44, Punctuation, '...'),
            (47, Text, ' '),
            (48, Keyword, 'on'),
            (50, Text, ' '),
            (51, Name.Class, 'Human'),
            (56, Text, ' '),
            (57, Punctuation, '{'),
            (58, Text, '\n      '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '}'),
            (75, Text, '\n    '),
            (80, Punctuation, '...'),
            (83, Text, ' '),
            (84, Keyword, 'on'),
            (86, Text, ' '),
            (87, Name.Class, 'Droid'),
            (92, Text, ' '),
            (93, Punctuation, '{'),
            (94, Text, '\n      '),
            (101, Name, 'name'),
            (105, Text, '\n    '),
            (110, Punctuation, '}'),
            (111, Text, '\n    '),
            (116, Punctuation, '...'),
            (119, Text, ' '),
            (120, Keyword, 'on'),
            (122, Text, ' '),
            (123, Name.Class, 'Starship'),
            (131, Text, ' '),
            (132, Punctuation, '{'),
            (133, Text, '\n      '),
            (140, Name, 'name'),
            (144, Text, '\n    '),
            (149, Punctuation, '}'),
            (150, Text, '\n  '),
            (153, Punctuation, '}'),
            (154, Text, '\n'),
            (155, Punctuation, '}'),
            (156, Text, '\n'),
        ]
        assert_tokens_equal(self, text, expected)

    def test_input_value_nested_array(self):
        text = '''\
        {
          node(arg: [1, [2, 3]])
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'node'),
            (8, Punctuation, '('),
            (9, Name, 'arg'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, Punctuation, '['),
            (15, Number.Integer, '1'),
            (16, Punctuation, ','),
            (17, Text, ' '),
            (18, Punctuation, '['),
            (19, Number.Integer, '2'),
            (20, Punctuation, ','),
            (21, Text, ' '),
            (22, Number.Integer, '3'),
            (23, Punctuation, ']'),
            (24, Punctuation, ']'),
            (25, Punctuation, ')'),
            (26, Text, '\n'),
            (27, Punctuation, '}'),
            (28, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_input_value_object(self):
        text = '''\
        {
          node(arg: {a: "a", b: "b"})
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'node'),
            (8, Punctuation, '('),
            (9, Name, 'arg'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, Punctuation, '{'),
            (15, Name, 'a'),
            (16, Punctuation, ':'),
            (17, Text, ' '),
            (18, String, '"'),
            (19, String, 'a'),
            (20, String, '"'),
            (21, Punctuation, ','),
            (22, Text, ' '),
            (23, Name, 'b'),
            (24, Punctuation, ':'),
            (25, Text, ' '),
            (26, String, '"'),
            (27, String, 'b'),
            (28, String, '"'),
            (29, Punctuation, '}'),
            (30, Punctuation, ')'),
            (31, Text, '\n'),
            (32, Punctuation, '}'),
            (33, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_input_nested_value_object(self):
        text = '''\
        {
          node(arg: {a: "a", b: {c: "c", d: {e: "e"}}})
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'node'),
            (8, Punctuation, '('),
            (9, Name, 'arg'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, Punctuation, '{'),
            (15, Name, 'a'),
            (16, Punctuation, ':'),
            (17, Text, ' '),
            (18, String, '"'),
            (19, String, 'a'),
            (20, String, '"'),
            (21, Punctuation, ','),
            (22, Text, ' '),
            (23, Name, 'b'),
            (24, Punctuation, ':'),
            (25, Text, ' '),
            (26, Punctuation, '{'),
            (27, Name, 'c'),
            (28, Punctuation, ':'),
            (29, Text, ' '),
            (30, String, '"'),
            (31, String, 'c'),
            (32, String, '"'),
            (33, Punctuation, ','),
            (34, Text, ' '),
            (35, Name, 'd'),
            (36, Punctuation, ':'),
            (37, Text, ' '),
            (38, Punctuation, '{'),
            (39, Name, 'e'),
            (40, Punctuation, ':'),
            (41, Text, ' '),
            (42, String, '"'),
            (43, String, 'e'),
            (44, String, '"'),
            (45, Punctuation, '}'),
            (46, Punctuation, '}'),
            (47, Punctuation, '}'),
            (48, Punctuation, ')'),
            (49, Text, '\n'),
            (50, Punctuation, '}'),
            (51, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_input_value_mixed(self):
        text = '''\
        {
          node1(arg: {a: [1, 2]})
          node2(arg: [1, {a: "a"}])
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'node1'),
            (9, Punctuation, '('),
            (10, Name, 'arg'),
            (13, Punctuation, ':'),
            (14, Text, ' '),
            (15, Punctuation, '{'),
            (16, Name, 'a'),
            (17, Punctuation, ':'),
            (18, Text, ' '),
            (19, Punctuation, '['),
            (20, Number.Integer, '1'),
            (21, Punctuation, ','),
            (22, Text, ' '),
            (23, Number.Integer, '2'),
            (24, Punctuation, ']'),
            (25, Punctuation, '}'),
            (26, Punctuation, ')'),
            (27, Text, '\n  '),
            (30, Name, 'node2'),
            (35, Punctuation, '('),
            (36, Name, 'arg'),
            (39, Punctuation, ':'),
            (40, Text, ' '),
            (41, Punctuation, '['),
            (42, Number.Integer, '1'),
            (43, Punctuation, ','),
            (44, Text, ' '),
            (45, Punctuation, '{'),
            (46, Name, 'a'),
            (47, Punctuation, ':'),
            (48, Text, ' '),
            (49, String, '"'),
            (50, String, 'a'),
            (51, String, '"'),
            (52, Punctuation, '}'),
            (53, Punctuation, ']'),
            (54, Punctuation, ')'),
            (55, Text, '\n'),
            (56, Punctuation, '}'),
            (57, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)


class TestExamplesWebsite(unittest.TestCase):

    maxDiff = None

    def test_fields1(self):
        # https://graphql.org/learn/queries/#fields
        text = '''\
        {
          hero {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'hero'),
            (8, Text, ' '),
            (9, Punctuation, '{'),
            (10, Text, '\n    '),
            (15, Name, 'name'),
            (19, Text, '\n  '),
            (22, Punctuation, '}'),
            (23, Text, '\n'),
            (24, Punctuation, '}'),
            (25, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_fields2(self):
        # https://graphql.org/learn/queries/#fields
        text = '''\
        {
          hero {
            name
            # Queries can have comments!
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'hero'),
            (8, Text, ' '),
            (9, Punctuation, '{'),
            (10, Text, '\n    '),
            (15, Name, 'name'),
            (19, Text, '\n    '),
            (24, Comment, '# Queries can have comments!'),
            (52, Text, '\n    '),
            (57, Name, 'friends'),
            (64, Text, ' '),
            (65, Punctuation, '{'),
            (66, Text, '\n      '),
            (73, Name, 'name'),
            (77, Text, '\n    '),
            (82, Punctuation, '}'),
            (83, Text, '\n  '),
            (86, Punctuation, '}'),
            (87, Text, '\n'),
            (88, Punctuation, '}'),
            (89, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_arguments1(self):
        # https://graphql.org/learn/queries/#arguments
        text = '''\
        {
          human(id: "1000") {
            name
            height
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'human'),
            (9, Punctuation, '('),
            (10, Name, 'id'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, String, '"'),
            (15, String, '1000'),
            (19, String, '"'),
            (20, Punctuation, ')'),
            (21, Text, ' '),
            (22, Punctuation, '{'),
            (23, Text, '\n    '),
            (28, Name, 'name'),
            (32, Text, '\n    '),
            (37, Name, 'height'),
            (43, Text, '\n  '),
            (46, Punctuation, '}'),
            (47, Text, '\n'),
            (48, Punctuation, '}'),
            (49, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_arguments2(self):
        # https://graphql.org/learn/queries/#arguments
        text = '''\
        {
          human(id: "1000") {
            name
            height(unit: FOOT)
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'human'),
            (9, Punctuation, '('),
            (10, Name, 'id'),
            (12, Punctuation, ':'),
            (13, Text, ' '),
            (14, String, '"'),
            (15, String, '1000'),
            (19, String, '"'),
            (20, Punctuation, ')'),
            (21, Text, ' '),
            (22, Punctuation, '{'),
            (23, Text, '\n    '),
            (28, Name, 'name'),
            (32, Text, '\n    '),
            (37, Name, 'height'),
            (43, Punctuation, '('),
            (44, Name, 'unit'),
            (48, Punctuation, ':'),
            (49, Text, ' '),
            (50, Name.Constant, 'FOOT'),
            (54, Punctuation, ')'),
            (55, Text, '\n  '),
            (58, Punctuation, '}'),
            (59, Text, '\n'),
            (60, Punctuation, '}'),
            (61, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_aliases1(self):
        # https://graphql.org/learn/queries/#aliases
        text = '''\
        {
          empireHero: hero(episode: EMPIRE) {
            name
          }
          jediHero: hero(episode: JEDI) {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name.Label, 'empireHero'),
            (14, Punctuation, ':'),
            (15, Text, ' '),
            (16, Name, 'hero'),
            (20, Punctuation, '('),
            (21, Name, 'episode'),
            (28, Punctuation, ':'),
            (29, Text, ' '),
            (30, Name.Constant, 'EMPIRE'),
            (36, Punctuation, ')'),
            (37, Text, ' '),
            (38, Punctuation, '{'),
            (39, Text, '\n    '),
            (44, Name, 'name'),
            (48, Text, '\n  '),
            (51, Punctuation, '}'),
            (52, Text, '\n  '),
            (55, Name.Label, 'jediHero'),
            (63, Punctuation, ':'),
            (64, Text, ' '),
            (65, Name, 'hero'),
            (69, Punctuation, '('),
            (70, Name, 'episode'),
            (77, Punctuation, ':'),
            (78, Text, ' '),
            (79, Name.Constant, 'JEDI'),
            (83, Punctuation, ')'),
            (84, Text, ' '),
            (85, Punctuation, '{'),
            (86, Text, '\n    '),
            (91, Name, 'name'),
            (95, Text, '\n  '),
            (98, Punctuation, '}'),
            (99, Text, '\n'),
            (100, Punctuation, '}'),
            (101, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_fragments1(self):
        # https://graphql.org/learn/queries/#fragments
        text = '''\
        {
          leftComparison: hero(episode: EMPIRE) {
            ...comparisonFields
          }
          rightComparison: hero(episode: JEDI) {
            ...comparisonFields
          }
        }

        fragment comparisonFields on Character {
          name
          appearsIn
          friends {
            name
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name.Label, 'leftComparison'),
            (18, Punctuation, ':'),
            (19, Text, ' '),
            (20, Name, 'hero'),
            (24, Punctuation, '('),
            (25, Name, 'episode'),
            (32, Punctuation, ':'),
            (33, Text, ' '),
            (34, Name.Constant, 'EMPIRE'),
            (40, Punctuation, ')'),
            (41, Text, ' '),
            (42, Punctuation, '{'),
            (43, Text, '\n    '),
            (48, Punctuation, '...'),
            (51, Name, 'comparisonFields'),
            (67, Text, '\n  '),
            (70, Punctuation, '}'),
            (71, Text, '\n  '),
            (74, Name.Label, 'rightComparison'),
            (89, Punctuation, ':'),
            (90, Text, ' '),
            (91, Name, 'hero'),
            (95, Punctuation, '('),
            (96, Name, 'episode'),
            (103, Punctuation, ':'),
            (104, Text, ' '),
            (105, Name.Constant, 'JEDI'),
            (109, Punctuation, ')'),
            (110, Text, ' '),
            (111, Punctuation, '{'),
            (112, Text, '\n    '),
            (117, Punctuation, '...'),
            (120, Name, 'comparisonFields'),
            (136, Text, '\n  '),
            (139, Punctuation, '}'),
            (140, Text, '\n'),
            (141, Punctuation, '}'),
            (142, Text, '\n\n'),
            (144, Keyword, 'fragment'),
            (152, Text, ' '),
            (153, Name.Function, 'comparisonFields'),
            (169, Text, ' '),
            (170, Keyword, 'on'),
            (172, Text, ' '),
            (173, Name.Class, 'Character'),
            (182, Text, ' '),
            (183, Punctuation, '{'),
            (184, Text, '\n  '),
            (187, Name, 'name'),
            (191, Text, '\n  '),
            (194, Name, 'appearsIn'),
            (203, Text, '\n  '),
            (206, Name, 'friends'),
            (213, Text, ' '),
            (214, Punctuation, '{'),
            (215, Text, '\n    '),
            (220, Name, 'name'),
            (224, Text, '\n  '),
            (227, Punctuation, '}'),
            (228, Text, '\n'),
            (229, Punctuation, '}'),
            (230, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_fragments2(self):
        # https://graphql.org/learn/queries/#fragments
        text = '''\
        query HeroComparison($first: Int = 3) {
          leftComparison: hero(episode: EMPIRE) {
            ...comparisonFields
          }
          rightComparison: hero(episode: JEDI) {
            ...comparisonFields
          }
        }

        fragment comparisonFields on Character {
          name
          friendsConnection(first: $first) {
            totalCount
            edges {
              node {
                name
              }
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroComparison'),
            (20, Punctuation, '('),
            (21, Name.Variable, '$first'),
            (27, Punctuation, ':'),
            (28, Text, ' '),
            (29, Name.Builtin, 'Int'),
            (32, Text, ' '),
            (33, Punctuation, '='),
            (34, Text, ' '),
            (35, Number.Integer, '3'),
            (36, Punctuation, ')'),
            (37, Text, ' '),
            (38, Punctuation, '{'),
            (39, Text, '\n  '),
            (42, Name.Label, 'leftComparison'),
            (56, Punctuation, ':'),
            (57, Text, ' '),
            (58, Name, 'hero'),
            (62, Punctuation, '('),
            (63, Name, 'episode'),
            (70, Punctuation, ':'),
            (71, Text, ' '),
            (72, Name.Constant, 'EMPIRE'),
            (78, Punctuation, ')'),
            (79, Text, ' '),
            (80, Punctuation, '{'),
            (81, Text, '\n    '),
            (86, Punctuation, '...'),
            (89, Name, 'comparisonFields'),
            (105, Text, '\n  '),
            (108, Punctuation, '}'),
            (109, Text, '\n  '),
            (112, Name.Label, 'rightComparison'),
            (127, Punctuation, ':'),
            (128, Text, ' '),
            (129, Name, 'hero'),
            (133, Punctuation, '('),
            (134, Name, 'episode'),
            (141, Punctuation, ':'),
            (142, Text, ' '),
            (143, Name.Constant, 'JEDI'),
            (147, Punctuation, ')'),
            (148, Text, ' '),
            (149, Punctuation, '{'),
            (150, Text, '\n    '),
            (155, Punctuation, '...'),
            (158, Name, 'comparisonFields'),
            (174, Text, '\n  '),
            (177, Punctuation, '}'),
            (178, Text, '\n'),
            (179, Punctuation, '}'),
            (180, Text, '\n\n'),
            (182, Keyword, 'fragment'),
            (190, Text, ' '),
            (191, Name.Function, 'comparisonFields'),
            (207, Text, ' '),
            (208, Keyword, 'on'),
            (210, Text, ' '),
            (211, Name.Class, 'Character'),
            (220, Text, ' '),
            (221, Punctuation, '{'),
            (222, Text, '\n  '),
            (225, Name, 'name'),
            (229, Text, '\n  '),
            (232, Name, 'friendsConnection'),
            (249, Punctuation, '('),
            (250, Name, 'first'),
            (255, Punctuation, ':'),
            (256, Text, ' '),
            (257, Name.Variable, '$first'),
            (263, Punctuation, ')'),
            (264, Text, ' '),
            (265, Punctuation, '{'),
            (266, Text, '\n    '),
            (271, Name, 'totalCount'),
            (281, Text, '\n    '),
            (286, Name, 'edges'),
            (291, Text, ' '),
            (292, Punctuation, '{'),
            (293, Text, '\n      '),
            (300, Name, 'node'),
            (304, Text, ' '),
            (305, Punctuation, '{'),
            (306, Text, '\n        '),
            (315, Name, 'name'),
            (319, Text, '\n      '),
            (326, Punctuation, '}'),
            (327, Text, '\n    '),
            (332, Punctuation, '}'),
            (333, Text, '\n  '),
            (336, Punctuation, '}'),
            (337, Text, '\n'),
            (338, Punctuation, '}'),
            (339, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_operation_name1(self):
        # https://graphql.org/learn/queries/#operation-name
        text = '''\
        query HeroNameAndFriends {
          hero {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Text, ' '),
            (25, Punctuation, '{'),
            (26, Text, '\n  '),
            (29, Name, 'hero'),
            (33, Text, ' '),
            (34, Punctuation, '{'),
            (35, Text, '\n    '),
            (40, Name, 'name'),
            (44, Text, '\n    '),
            (49, Name, 'friends'),
            (56, Text, ' '),
            (57, Punctuation, '{'),
            (58, Text, '\n      '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '}'),
            (75, Text, '\n  '),
            (78, Punctuation, '}'),
            (79, Text, '\n'),
            (80, Punctuation, '}'),
            (81, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_variables1(self):
        # https://graphql.org/learn/queries/#variables
        text = '''\
        query HeroNameAndFriends($episode: Episode) {
          hero(episode: $episode) {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Punctuation, '('),
            (25, Name.Variable, '$episode'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Class, 'Episode'),
            (42, Punctuation, ')'),
            (43, Text, ' '),
            (44, Punctuation, '{'),
            (45, Text, '\n  '),
            (48, Name, 'hero'),
            (52, Punctuation, '('),
            (53, Name, 'episode'),
            (60, Punctuation, ':'),
            (61, Text, ' '),
            (62, Name.Variable, '$episode'),
            (70, Punctuation, ')'),
            (71, Text, ' '),
            (72, Punctuation, '{'),
            (73, Text, '\n    '),
            (78, Name, 'name'),
            (82, Text, '\n    '),
            (87, Name, 'friends'),
            (94, Text, ' '),
            (95, Punctuation, '{'),
            (96, Text, '\n      '),
            (103, Name, 'name'),
            (107, Text, '\n    '),
            (112, Punctuation, '}'),
            (113, Text, '\n  '),
            (116, Punctuation, '}'),
            (117, Text, '\n'),
            (118, Punctuation, '}'),
            (119, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_variables2(self):
        # https://graphql.org/learn/queries/#variables
        text = '''\
        query HeroNameAndFriends($episode: Episode = JEDI) {
          hero(episode: $episode) {
            name
            friends {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroNameAndFriends'),
            (24, Punctuation, '('),
            (25, Name.Variable, '$episode'),
            (33, Punctuation, ':'),
            (34, Text, ' '),
            (35, Name.Class, 'Episode'),
            (42, Text, ' '),
            (43, Punctuation, '='),
            (44, Text, ' '),
            (45, Name.Constant, 'JEDI'),
            (49, Punctuation, ')'),
            (50, Text, ' '),
            (51, Punctuation, '{'),
            (52, Text, '\n  '),
            (55, Name, 'hero'),
            (59, Punctuation, '('),
            (60, Name, 'episode'),
            (67, Punctuation, ':'),
            (68, Text, ' '),
            (69, Name.Variable, '$episode'),
            (77, Punctuation, ')'),
            (78, Text, ' '),
            (79, Punctuation, '{'),
            (80, Text, '\n    '),
            (85, Name, 'name'),
            (89, Text, '\n    '),
            (94, Name, 'friends'),
            (101, Text, ' '),
            (102, Punctuation, '{'),
            (103, Text, '\n      '),
            (110, Name, 'name'),
            (114, Text, '\n    '),
            (119, Punctuation, '}'),
            (120, Text, '\n  '),
            (123, Punctuation, '}'),
            (124, Text, '\n'),
            (125, Punctuation, '}'),
            (126, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_directives1(self):
        # https://graphql.org/learn/queries/#directives
        text = '''\
        query Hero($episode: Episode, $withFriends: Boolean!) {
          hero(episode: $episode) {
            name
            friends @include(if: $withFriends) {
              name
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'Hero'),
            (10, Punctuation, '('),
            (11, Name.Variable, '$episode'),
            (19, Punctuation, ':'),
            (20, Text, ' '),
            (21, Name.Class, 'Episode'),
            (28, Punctuation, ','),
            (29, Text, ' '),
            (30, Name.Variable, '$withFriends'),
            (42, Punctuation, ':'),
            (43, Text, ' '),
            (44, Name.Builtin, 'Boolean'),
            (51, Punctuation, '!'),
            (52, Punctuation, ')'),
            (53, Text, ' '),
            (54, Punctuation, '{'),
            (55, Text, '\n  '),
            (58, Name, 'hero'),
            (62, Punctuation, '('),
            (63, Name, 'episode'),
            (70, Punctuation, ':'),
            (71, Text, ' '),
            (72, Name.Variable, '$episode'),
            (80, Punctuation, ')'),
            (81, Text, ' '),
            (82, Punctuation, '{'),
            (83, Text, '\n    '),
            (88, Name, 'name'),
            (92, Text, '\n    '),
            (97, Name, 'friends'),
            (104, Text, ' '),
            (105, Name.Decorator, '@include'),
            (113, Punctuation, '('),
            (114, Name, 'if'),
            (116, Punctuation, ':'),
            (117, Text, ' '),
            (118, Name.Variable, '$withFriends'),
            (130, Punctuation, ')'),
            (131, Text, ' '),
            (132, Punctuation, '{'),
            (133, Text, '\n      '),
            (140, Name, 'name'),
            (144, Text, '\n    '),
            (149, Punctuation, '}'),
            (150, Text, '\n  '),
            (153, Punctuation, '}'),
            (154, Text, '\n'),
            (155, Punctuation, '}'),
            (156, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_mutations1(self):
        # https://graphql.org/learn/queries/#mutations
        text = '''\
        mutation CreateReviewForEpisode($ep: Episode!, $review: ReviewInput!) {
          createReview(episode: $ep, review: $review) {
            stars
            commentary
          }
        }
        '''
        expected = [
            (0, Keyword, 'mutation'),
            (8, Text, ' '),
            (9, Name.Function, 'CreateReviewForEpisode'),
            (31, Punctuation, '('),
            (32, Name.Variable, '$ep'),
            (35, Punctuation, ':'),
            (36, Text, ' '),
            (37, Name.Class, 'Episode'),
            (44, Punctuation, '!'),
            (45, Punctuation, ','),
            (46, Text, ' '),
            (47, Name.Variable, '$review'),
            (54, Punctuation, ':'),
            (55, Text, ' '),
            (56, Name.Class, 'ReviewInput'),
            (67, Punctuation, '!'),
            (68, Punctuation, ')'),
            (69, Text, ' '),
            (70, Punctuation, '{'),
            (71, Text, '\n  '),
            (74, Name, 'createReview'),
            (86, Punctuation, '('),
            (87, Name, 'episode'),
            (94, Punctuation, ':'),
            (95, Text, ' '),
            (96, Name.Variable, '$ep'),
            (99, Punctuation, ','),
            (100, Text, ' '),
            (101, Name, 'review'),
            (107, Punctuation, ':'),
            (108, Text, ' '),
            (109, Name.Variable, '$review'),
            (116, Punctuation, ')'),
            (117, Text, ' '),
            (118, Punctuation, '{'),
            (119, Text, '\n    '),
            (124, Name, 'stars'),
            (129, Text, '\n    '),
            (134, Name, 'commentary'),
            (144, Text, '\n  '),
            (147, Punctuation, '}'),
            (148, Text, '\n'),
            (149, Punctuation, '}'),
            (150, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_inline_fragments1(self):
        # https://graphql.org/learn/queries/#inline-fragments
        text = '''\
        query HeroForEpisode($ep: Episode!) {
          hero(episode: $ep) {
            name
            ... on Droid {
              primaryFunction
            }
            ... on Human {
              height
            }
          }
        }
        '''
        expected = [
            (0, Keyword, 'query'),
            (5, Text, ' '),
            (6, Name.Function, 'HeroForEpisode'),
            (20, Punctuation, '('),
            (21, Name.Variable, '$ep'),
            (24, Punctuation, ':'),
            (25, Text, ' '),
            (26, Name.Class, 'Episode'),
            (33, Punctuation, '!'),
            (34, Punctuation, ')'),
            (35, Text, ' '),
            (36, Punctuation, '{'),
            (37, Text, '\n  '),
            (40, Name, 'hero'),
            (44, Punctuation, '('),
            (45, Name, 'episode'),
            (52, Punctuation, ':'),
            (53, Text, ' '),
            (54, Name.Variable, '$ep'),
            (57, Punctuation, ')'),
            (58, Text, ' '),
            (59, Punctuation, '{'),
            (60, Text, '\n    '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '...'),
            (77, Text, ' '),
            (78, Keyword, 'on'),
            (80, Text, ' '),
            (81, Name.Class, 'Droid'),
            (86, Text, ' '),
            (87, Punctuation, '{'),
            (88, Text, '\n      '),
            (95, Name, 'primaryFunction'),
            (110, Text, '\n    '),
            (115, Punctuation, '}'),
            (116, Text, '\n    '),
            (121, Punctuation, '...'),
            (124, Text, ' '),
            (125, Keyword, 'on'),
            (127, Text, ' '),
            (128, Name.Class, 'Human'),
            (133, Text, ' '),
            (134, Punctuation, '{'),
            (135, Text, '\n      '),
            (142, Name, 'height'),
            (148, Text, '\n    '),
            (153, Punctuation, '}'),
            (154, Text, '\n  '),
            (157, Punctuation, '}'),
            (158, Text, '\n'),
            (159, Punctuation, '}'),
            (160, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)

    def test_inline_fragments2(self):
        # https://graphql.org/learn/queries/#meta-fields
        text = '''\
        {
          search(text: "an") {
            __typename
            ... on Human {
              name
            }
            ... on Droid {
              name
            }
            ... on Starship {
              name
            }
          }
        }
        '''
        expected = [
            (0, Punctuation, '{'),
            (1, Text, '\n  '),
            (4, Name, 'search'),
            (10, Punctuation, '('),
            (11, Name, 'text'),
            (15, Punctuation, ':'),
            (16, Text, ' '),
            (17, String, '"'),
            (18, String, 'an'),
            (20, String, '"'),
            (21, Punctuation, ')'),
            (22, Text, ' '),
            (23, Punctuation, '{'),
            (24, Text, '\n    '),
            (29, Name, '__typename'),
            (39, Text, '\n    '),
            (44, Punctuation, '...'),
            (47, Text, ' '),
            (48, Keyword, 'on'),
            (50, Text, ' '),
            (51, Name.Class, 'Human'),
            (56, Text, ' '),
            (57, Punctuation, '{'),
            (58, Text, '\n      '),
            (65, Name, 'name'),
            (69, Text, '\n    '),
            (74, Punctuation, '}'),
            (75, Text, '\n    '),
            (80, Punctuation, '...'),
            (83, Text, ' '),
            (84, Keyword, 'on'),
            (86, Text, ' '),
            (87, Name.Class, 'Droid'),
            (92, Text, ' '),
            (93, Punctuation, '{'),
            (94, Text, '\n      '),
            (101, Name, 'name'),
            (105, Text, '\n    '),
            (110, Punctuation, '}'),
            (111, Text, '\n    '),
            (116, Punctuation, '...'),
            (119, Text, ' '),
            (120, Keyword, 'on'),
            (122, Text, ' '),
            (123, Name.Class, 'Starship'),
            (131, Text, ' '),
            (132, Punctuation, '{'),
            (133, Text, '\n      '),
            (140, Name, 'name'),
            (144, Text, '\n    '),
            (149, Punctuation, '}'),
            (150, Text, '\n  '),
            (153, Punctuation, '}'),
            (154, Text, '\n'),
            (155, Punctuation, '}'),
            (156, Text, '\n')
        ]
        assert_tokens_equal(self, text, expected)
